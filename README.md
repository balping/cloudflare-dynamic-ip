# Cloudflare dynamic IP

This systemd service automatically updates a DNS record at
Cloudflare's name server when your public IP changes.

## Install

System dependencies: libssl3

Run

```
cargo build --release
```

to compile the binary.

Copy the binary to `/usr/bin/cloudflareip`:

```
cp target/release/cloudflareip  /usr/bin/
chmod 755 /usr/bin/cloudflareip
```

Copy `cloudflareip.service` to `/lib/systemd/system`:

```
cp cloudflareip.service /lib/systemd/system/cloudflareip.service
```

Note: the folder might be `/etc/systemd/system/` depending on your distro.

Create a technical user and group for the service:

```
useradd cloudflareipservice -s /sbin/nologin -M
groupadd cloudflareipservice
usermod -a -G cloudflareipservice cloudflareipservice

```

Copy the example config file to /etc and modify its contents. (See section [Config](#config) below)

```
cp example_config.json /etc/cloudflare_ip.json
chown root:cloudflareipservice /etc/cloudflare_ip.json
chmod 640 /etc/cloudflare_ip.json
nano /etc/cloudflare_ip.json
```

Enable and start service:

```
systemctl enable cloudflareip.service
systemctl start cloudflareip.service
```

Check logs:

```
journalctl -f -u cloudflareip
```

## Config

* `token`: API key from Cloudflare.Consider restricting the
  key to minimal permissions required to edit a DNS zone.
* `domain`: the (sub-)domain you want to use for dynamic IP.
  This A record will be updated automatically. You need to create an
  A record manually on Cloudflare's web user interface first.
  Eg.: dynip.example.com
* `zone`: the zone that the above domain belongs to. Eg.: example.com
* `checkFrequency`: the service will poll your public ip in every `checkFrequency` seconds.

## License

GPL v3.0 or later.