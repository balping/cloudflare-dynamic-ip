/**
	CloudflareIP: automatically updates a DNS record at
	Cloudflare's name server when your public IP changes.

    Copyright (C) 2023  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

mod cloudflare_api;

use std::error::Error;
use std::net::{IpAddr, Ipv4Addr};
use std::net::ToSocketAddrs;
use std::rc::Rc;
use std::{thread, time::Duration};
use log::{error, warn, info};


#[macro_use] extern crate quick_error;


fn get_ip(client : Rc<reqwest::blocking::Client>) -> Result<Ipv4Addr, Box<dyn Error>> {
	let request_builder = client.get("https://api.ipify.org");
	let resp = request_builder.send()?.text()?;
	let ip : Ipv4Addr = resp.parse()?;
	Ok(ip)
}

fn resolve_domain(domain : &str) -> Result<Ipv4Addr, &'static str>  {
	match format!("{}:443",domain).to_socket_addrs() {
		Ok(mut addresses) => match addresses.next().ok_or("no ip found for domain")?.ip() {
				IpAddr::V4(ipv4) => Ok(ipv4),
				IpAddr::V6(_) => Err("No ipv4 could be resolved")
			},
		Err(_) => Err("Domain cannot be resolved")
	}
}


fn main() ->  Result<(),()>{
	env_logger::init();

	info!("Cloudflare IP service started.");

	let client = Rc::new(reqwest::blocking::Client::new());

	let mut cloudflare_client = cloudflare_api::CloudflareClient::new(Rc::clone(&client));



	match cloudflare_client.load_config() {
		Ok(_) => {}
		Err(e) => {
			error!("Failed to load the config file: {}", e.to_string());
			return Err(());
		}
	};

	match cloudflare_client.get_zone_id() {
		Ok(_) => {}
		Err(e) => {
			error!("Failed to obtain the zone ID: {}", e.to_string());
			return Err(());
		}
	}
	match cloudflare_client.get_record_id() {
		Ok(_) => {}
		Err(e) => {
			error!("Failed to obtain the record ID: {}", e.to_string());
			return Err(());
		}
	}


	loop {
		match get_ip(Rc::clone(&client)) {
			Ok(new_ip) => {
				if Some(&new_ip) != cloudflare_client.record_ip.as_ref() {
					match cloudflare_client.update_record(&new_ip) {
						Ok(_) => {info!("New IP: {}", new_ip.to_string())}
						Err(e) => {warn!("Failed to update the record with the new ip: {}", e.to_string());}
					};
				} else {
					// println!("no new ip");
				}
			},
			Err(_) => {
				warn!("Failed to request the current ip");
			}
		}

		thread::sleep(Duration::from_secs(cloudflare_client.frequency));
	}

	return Ok(());
}