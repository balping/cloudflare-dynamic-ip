/**
	CloudflareIP: automatically updates a DNS record at
	Cloudflare's name server when your public IP changes.

    Copyright (C) 2023  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::error::Error;
use std::rc::Rc;
use std::string::ToString;
use serde_json::json;
use std::net::Ipv4Addr;
use chrono;
use std::env;
use std::fs;



quick_error! {
    #[derive(Debug)]
    pub enum ApiError {
		Unsuccessful {}
		NotASingleResult {}
		IdNotFound {}
		ContentNotFound {}
	}
}

quick_error! {
    #[derive(Debug)]
    pub enum ConfigError {
		InvalidToken {}
		InvalidZone {}
		InvalidDomain {}
		InvalidFrequency {}
	}
}

pub struct CloudflareClient {
	pub reqwest_client: Rc<reqwest::blocking::Client>,
	zone_id: String,
	record_id: String,
	pub record_ip: Option<Ipv4Addr>,
	token: String,
	zone: String,
	domain: String,
	pub frequency: u64,
}

impl CloudflareClient {
	pub fn new(reqwest_client: Rc<reqwest::blocking::Client>) -> Self {
		CloudflareClient {
			reqwest_client,
			zone_id : "".to_string(),
			record_id : "".to_string(),
			record_ip: None,
			token: "".to_string(),
			zone: "".to_string(),
			domain: "".to_string(),
			frequency: 60,
		}
	}

	pub fn get_zone_id(&mut self) -> Result<(), Box<dyn Error>> {
		let resp : serde_json::Value = self.reqwest_client.get("https://api.cloudflare.com/client/v4/zones")
			.query(&[("name", self.zone.as_str())])
			.header("Content-Type","application/json")
			.header("Authorization", format!("Bearer {}", self.token))
			.send()?.json()?;

		if resp["success"].as_bool() != Some(true) {
			return Err(Box::from(ApiError::Unsuccessful));
		}

		if resp["result_info"]["count"].as_i64().unwrap_or(0) != 1 {
			return Err(Box::from(ApiError::NotASingleResult));
		}

		let id = resp["result"][0]["id"].as_str();

		match id {
			None => Err(Box::from(ApiError::IdNotFound)),
			Some(id) => {
				self.zone_id = id.to_string();
				Ok(())
			}
		}
	}

	pub fn get_record_id(&mut self) -> Result<(), Box<dyn Error>> {
		let resp : serde_json::Value = self.reqwest_client.get(format!("https://api.cloudflare.com/client/v4/zones/{}/dns_records", self.zone_id))
			.query(&[("name", self.domain.as_str()), ("type", "A")])
			.header("Content-Type","application/json")
			.header("Authorization", format!("Bearer {}", self.token))
			.send()?.json()?;

		if resp["success"].as_bool() != Some(true) {
			return Err(Box::from(ApiError::Unsuccessful));
		}

		if resp["result_info"]["count"].as_i64().unwrap_or(0) != 1 {
			return Err(Box::from(ApiError::NotASingleResult));
		}

		let id = resp["result"][0]["id"].as_str();

		match id {
			None => return Err(Box::from(ApiError::IdNotFound)),
			Some(id) => {
				self.record_id = id.to_string();
			}
		};

		match resp["result"][0]["content"].as_str() {
			None => return Err(Box::from(ApiError::ContentNotFound)),
			Some(ip) => {
				let ip : Ipv4Addr = ip.parse()?;
				self.record_ip = Some(ip);
			}
		};

		Ok(())
	}

	pub fn update_record(&mut self, new_ip: &Ipv4Addr) -> Result<(), Box<dyn Error>> {
		let params = json!({
			"name": self.domain,
			"type": "A",
			"content":  new_ip.to_string().as_str(),
			"proxied": false,
			"ttl": 60,
			"comment": format!("Updated automatically at {:?} (UTC)", chrono::offset::Utc::now())
		});
		let resp : serde_json::Value = self.reqwest_client.put(format!("https://api.cloudflare.com/client/v4/zones/{}/dns_records/{}", self.zone_id, self.record_id))
			.json(&params)
			.header("Authorization", format!("Bearer {}", self.token))
			.send()?.json()?;

		if resp["success"].as_bool() != Some(true) {
			return Err(Box::from(ApiError::Unsuccessful));
		}


		match resp["result"]["content"].as_str() {
			None => return Err(Box::from(ApiError::ContentNotFound)),
			Some(ip) => {
				let ip : Ipv4Addr = ip.parse()?;
				self.record_ip = Some(ip);
			}
		};

		Ok(())
	}

	pub fn load_config(&mut self) -> Result<(), Box<dyn Error>> {
		let mut config_path = "/etc/cloudflare_ip.json";

		let args: Vec<String> = env::args().collect();
		if args.len() >= 3 && args[1] == "--config" {
			config_path = &args[2];
		}

		let config : serde_json::Value = fs::read_to_string(config_path)?.parse()?;


		self.token = match config["token"].as_str() {
			None => return Err(Box::from(ConfigError::InvalidToken)),
			Some(token) => token.to_string()
		};

		self.zone = match config["zone"].as_str() {
			None => return Err(Box::from(ConfigError::InvalidZone)),
			Some(zone) => zone.to_string()
		};

		self.domain = match config["domain"].as_str() {
			None => return Err(Box::from(ConfigError::InvalidDomain)),
			Some(domain) => domain.to_string()
		};

		self.frequency = match config["checkFrequency"].as_u64() {
			None => return Err(Box::from(ConfigError::InvalidFrequency)),
			Some(frequency) => frequency
		};


		Ok(())
	}

}


